const express = require("express");
const router = express.Router();
require('dotenv').config({ path: '.env' });
const { getALL } = require("../db/conexion");
const BASE_URL = "http://localhost:3000";

// Rutas del proyecto
router.get("/", async (request, response) => {
  // Obtener todos los integrantes desde la base de datos
  const integrantes = await getALL("SELECT * FROM Integrante WHERE esta_borrado = FALSE");
  // Definir la matrícula del home
  const matriculaDeHome = "Home";
  // Obtener los datos del home desde la base de datos
  const Home = await getALL(`SELECT * FROM media WHERE matricula = '${matriculaDeHome}'`);
  // Renderizar la página index con los datos del home y los integrantes
  response.render("index", {
    data: Home,
    BASE_URL,
    integrantes: integrantes,
    // Se traen los datos para el footer
    ENLACE: process.env.ENLACE,
    NOMBRE: process.env.NOMBRE,
    APELLIDO: process.env.APELLIDO,
    MATERIA: process.env.MATERIA
  });
});

router.get("/integrantes/:matricula", async (request, response) => {
  const matricula = request.params.matricula;
  // Obtener datos del integrante de la base de datos
  const integrantes = await getALL(`SELECT * FROM Integrante WHERE esta_borrado = FALSE`);
  // Obtener datos asociados al integrante de la base de datos
  const datos = await getALL(`SELECT * FROM media WHERE matricula = '${matricula}'`);
  if (integrantes.length === 0) {
    response.status(404).render("error");
  } else {
    response.render("integrante", {
      data: datos,
      BASE_URL,
      integrantes: integrantes
    });
  }
});


router.get("/paginas/word_cloud.html", async (request, response) => {
    // Obtener datos de integrantes de la base de datos
    const integrantes = await getALL("SELECT * FROM integrante");
    response.render("word_cloud",{
        integrantes: integrantes,
    });
});
router.get("/paginas/curso.html", async (request, response) => {
    // Obtener datos de integrantes de la base de datos
    const integrantes = await getALL("SELECT * FROM integrante");
    response.render("curso",{
        integrantes: integrantes,
    });
});
//exportar el router
module.exports=router;

